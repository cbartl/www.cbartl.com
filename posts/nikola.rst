.. title: Nikola Static Site Generator
.. slug: nikola
.. date: 2017-02-23 17:38:58 UTC+01:00
.. tags: nikola,blog
.. category: sally
.. link: 
.. description: How I use Nikola static site generator
.. type: text


Documentation
=============

**Nikola**

* `Getting started <https://getnikola.com/getting-started.html>`__
* `Handbook <https://www.getnikola.com/handbook.html>`__

**reStructuredText**

* `Primer <https://getnikola.com/quickstart.html>`__
* `Reference <https://getnikola.com/quickref.html>`__


*Nikola* command
================

Switch to nikola virtualenv
---------------------------
::

    cd ~/src/nikola
    . bin/activate



Build and Run (developer)
-------------------------

::

    nikola build
    nikola serve

Visit for development on http://127.0.0.1:8000


Build and Deploay (production)
------------------------------

Host *uberspace* must be configured in ~/.ssh/config for password-less login::

    nikola build
    nikola serve


Add new blog post
-----------------

::

    nikola new_page


Remove orphaned files
---------------------

After deleting or renaming posts old build artefacts remain. You can list and remove these orphans with::

    nikola orphans
    nikola check --clean-files

or simply::

    nikola clean
    nikola build



Manage Content
==============


Edit blog posts or pages
------------------------

Use your favorite editor. The files are located in:

+------------+--------+
| Blog posts | posts/ |
+------------+--------+
| Pages      | pages/ |
+------------+--------+


Translate post/page
-------------------

* Copy existing file
* Rename it to <name>.<language>.<suffix>
  Language may be *de* or *en*.
  Default is *de*.


My Theme
========

`Creating a theme <https://www.getnikola.com/creating-a-theme.html>`__

+---------------------------------+-----------------------------------+
| Install base theme              | nikola -i zen-jinja               |
+---------------------------------+-----------------------------------+
| Create new theme                | nikola theme -n bac-jinja \       |
| based on zen-jinja              | --parent zen-jinja --engine jinja |
+---------------------------------+-----------------------------------+
